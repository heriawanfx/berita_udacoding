import 'package:flutter/foundation.dart';
import 'package:flutter_berita_udacoding/model/article.dart';
import 'package:flutter_berita_udacoding/util/constant.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class HeadlineRepository {
  static Future<List<Article>> getArticles() async {
    final queryParams = {
      'country': Constant.country,
      'category': Constant.categoryTechnology,
      'apiKey': Constant.apiKey,
    };

    Uri uri = Uri.https(Constant.baseUrl, Constant.pathHeadlines, queryParams);
    http.Response response = await http.get(uri);

    if (response.statusCode == 200) {
      return compute(getParsedList, response.body);
    } else {
      throw Exception("Ada masalah dalam memuat berita");
    }
  }

  static List<Article> getParsedList(String responseBody) {
    Map<String, dynamic> jsonBody = jsonDecode(responseBody);
    List<dynamic> array = jsonBody['articles'];
    Iterable<Article> iterable = array.map((item) => Article.fromJson(item));
    List<Article> list = iterable.toList();
    return list;
  }
}
