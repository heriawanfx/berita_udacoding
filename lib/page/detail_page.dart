import 'package:flutter/material.dart';
import 'package:flutter_berita_udacoding/model/article.dart';

class DetailPage extends StatelessWidget {
  static const routeName = '/detail';

  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final article = ModalRoute.of(context)?.settings.arguments as Article;

    Widget _articleImage = article.urlToImage != null
        ? Hero(
            tag: "${article.urlToImage}",
            child: Image.network("${article.urlToImage}"))
        : Container(
            height: 250,
            color: Colors.grey,
          );

    return Scaffold(
      body: Column(
        children: <Widget>[
          _articleImage,
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${article.title}",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 8,
                ),
                Container(
                  padding: EdgeInsets.all(6.0),
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  child: Text(
                    "${article.source?.name}",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text("${article.content}"),
                Divider(),
                Text("Author: ${article.author}"),
                Text("${article.url}")
              ],
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {Navigator.pop(context)},
        child: const Icon(Icons.close),
        backgroundColor: Colors.green[900],
      ),
    );
  }
}
