import 'package:flutter/material.dart';
import 'package:flutter_berita_udacoding/model/article.dart';
import 'package:flutter_berita_udacoding/page/detail_page.dart';
import 'package:flutter_berita_udacoding/repository/headline_repository.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late Future<List<Article>> _future;

  @override
  void initState() {
    super.initState();
    _future = HeadlineRepository.getArticles();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: const Text("Berita Udacoding"),
        ),
        backgroundColor: Colors.green[900],
      ),
      body: FutureBuilder<List<Article>>(
        future: _future,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Article>? articles = snapshot.data;

            if (articles != null) {
              return _articleListView(articles);
            }
          }

          if (snapshot.hasError) {
            return Center(
              child: Text("${snapshot.error}"),
            );
          }

          return Center(child: const CircularProgressIndicator());
        },
      ),
    );
  }

  ListView _articleListView(List<Article> articles) {
    return ListView.builder(
        itemCount: articles.length,
        itemBuilder: (context, index) {
          Article article = articles[index];

          return ListTile(
            leading: _articleImage(article),
            title: Text(
              '${article.title}',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            subtitle: Text(
              '${article.description}',
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
            onTap: () => _pushToDetail(context, article),
            minVerticalPadding: 8,
          );
        });
  }

  Widget _articleImage(Article article) {
    return Container(
        color: Colors.grey,
        height: 50,
        width: 50,
        child: article.urlToImage != null
            ? Hero(
                tag: "${article.urlToImage}",
                child: Image.network(
                  "${article.urlToImage}",
                  width: 100,
                  fit: BoxFit.cover,
                ),
              )
            : Container(
                height: 100,
                color: Colors.grey,
              ));
  }

  void _pushToDetail(BuildContext context, Article article) {
    Navigator.pushNamed(context, DetailPage.routeName, arguments: article);
  }
}
