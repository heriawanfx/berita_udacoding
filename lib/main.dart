import 'package:flutter/material.dart';
import 'package:flutter_berita_udacoding/page/detail_page.dart';
import 'package:flutter_berita_udacoding/page/home_page.dart';

void main() {
  runApp(BeritaApp());
}

class BeritaApp extends StatelessWidget {
  const BeritaApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: const HomePage(),
      routes: {
        DetailPage.routeName: (c) => DetailPage(),
      },
    );
  }
}
