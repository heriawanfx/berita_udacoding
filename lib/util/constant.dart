class Constant {
  static const baseUrl = "newsapi.org";
  static const country = "id";
  static const apiKey = "2b3715a952b848439cacaf618b140246";
  static const pathHeadlines = "v2/top-headlines";
  static const categoryTechnology = "technology";
}
